package net.shajaran.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"net.shajaran"})
public class ExampleBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleBotApplication.class, args);
	}

}
