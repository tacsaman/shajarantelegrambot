package net.shajaran.bot.updateshandlers;

import net.shajaran.bot.api.City;
import net.shajaran.bot.api.Province;
import net.shajaran.bot.commands.HelpCommand;
import net.shajaran.bot.commands.MemberCommand;
import net.shajaran.bot.commands.StartCommand;
import net.shajaran.bot.commands.StopCommand;
import net.shajaran.bot.services.IpinbarServices;
import net.shajaran.bot.structure.*;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


/**
 * This handler mainly works with commands to demonstrate the Commands feature of the API
 *
 * @author Timo Schulz (Mit0x2)
 */
@Component
public class CommandsHandler extends TelegramLongPollingCommandBot implements ApplicationContextAware {
    private MemberCommand memberCommand;
    private IpinBarUserDao ipinBarUserDao = null;
    private IpinbarServices ipinbarServices = null;
    private InlineKeyboardMenuMaker inlineView;
    Logger logger = Logger.getLogger(CommandsHandler.class);
    ApplicationContext context;
    private KeyboardMenuMaker mainMenuHandler;
    private Integer messageId;

    @PostConstruct
    public void init() {
        memberCommand = context.getBean(MemberCommand.class);
        ipinBarUserDao = context.getBean(IpinBarUserDao.class);
        ipinbarServices = context.getBean(IpinbarServices.class);
        inlineView = context.getBean(InlineKeyboardMenuMaker.class);
        register(memberCommand);
        register(new StartCommand());
        register(new StopCommand());
        HelpCommand helpCommand = new HelpCommand(this);
        register(helpCommand);

        registerDefaultAction((absSender, message) -> {
            SendMessage commandUnknownMessage = new SendMessage();
            commandUnknownMessage.setChatId(message.getChatId());
            commandUnknownMessage.setText("The command '" + message.getText() + "' is not known by this bot. Here comes some help " + Emoji.AMBULANCE);
            try {
                absSender.sendMessage(commandUnknownMessage);
            } catch (TelegramApiException e) {
                logger.error(e);
            }
            helpCommand.execute(absSender, message.getFrom(), message.getChat(), new String[]{});
        });
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            Integer userId = message.getFrom().getId();
            States curentState = UserStateMap.getInstance().get(userId);
            mainMenuHandler = context.getBean(KeyboardMenuMaker.class);
            if (message.getText() != null) {
                if (message.getText().startsWith(ConstantsText.PERSONALPRODUCTOWNER)) {
                    onPersonalProductOwnerClick(message);
                } else if (message.getText().startsWith(ConstantsText.PRODUCTINDICATE)) {
                    onProductIndicateClick(message);
                } else if (message.getText().startsWith(ConstantsText.PERSONALDRIVER)) {
                    onPersonalDriverClic(message);
                } else if (message.getText().startsWith(ConstantsText.LEGALDRIVER)) {
                    onLegalDriverClick(message);
                } else {
                    handleIncomingMessage(update.getMessage());
                }
            } else if (message.getContact() != null) {
                onContactButtonReceive(message);
            }
        } else if (update.hasCallbackQuery()) {
            CallbackQuery callbackquery = update.getCallbackQuery();
            String[] data = callbackquery.getData().split(":");
            if (data[0].equals(States.PERSONALPRODUCTOWNER.name())) {
                if (data[1] != null) {
                    personalProductOwnerCallback(callbackquery, data[1]);
                }
            } else {
                long id = callbackquery.getFrom().getId().longValue();
                IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id);
                if (ipinBarUser == null) {
                    ipinBarUser = new IpinBarUser(id);
                }
                ipinBarUser.setCurrentState(States.MAINMENUE.name());
                changeMainMenu(ConstantsText.MAINMENU_SELECT, callbackquery.getMessage().getChatId(), KeyboardMenuMaker.mainMenu());
            }
        }
    }

    private void onContactButtonReceive(Message message) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(message.getFrom().getId().longValue());
        InlineKeyboardMarkup markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.NATIONAIDENTIFIER, States.PROVINCE);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.NATIONAIDENTIFIER);
        ipinBarUser.setPhone(message.getContact().getPhoneNumber());
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.NATIONALIDENTIFER, message.getChatId(), markup);
    }

    private void onPhoneMessageReceive(String phoneMessage, Integer userId, Long chatId) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(userId.longValue());
        InlineKeyboardMarkup markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.PHONE, States.PROVINCE);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.NATIONAIDENTIFIER);
        ipinBarUser.setPhone(phoneMessage);
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.NATIONALIDENTIFER, chatId, markup);
    }

    private void handleIncomingMessage(Message message) {
        long id = message.getFrom().getId().longValue();
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id);
        if (ipinBarUser != null) {
            String[] curentState = ipinBarUser.getCurrentState().split(":");
            if (curentState[0].equals(States.PERSONALPRODUCTOWNER.name())) {
                handlePersonalProductOwnerMessage(message, curentState[1]);
            }
        }
    }

    private void handlePersonalProductOwnerMessage(Message message, String state) {
        if (state.equals(States.NAME.name())) {
            onNameFamilyMessageReceive(message.getText(), message.getFrom().getId(), message.getChatId());
        } else if (state.equals(States.PHONE.name())) {
            onPhoneMessageReceive(message.getText(), message.getFrom().getId(), message.getChatId());
        } else if (state.equals(States.NATIONAIDENTIFIER.name())) {
            onNationalIdentifierMessageReceive(message.getText(), message.getFrom().getId(), message.getChatId());
        } else if (state.equals(States.PROVINCE.name())) {
            onProvinceMessageReceive(message.getText(), message.getFrom().getId(), message.getChatId());
        } else if (state.equals(States.CITY.name())) {
            onCityMessageReceive(message.getText(), message.getFrom().getId(), message.getChatId());
        } else if (state.equals(States.ACTIVATION_CODE.name())) {
            onActivationCodeMessageReceive(message.getText(), message.getFrom().getId(), message.getChatId());
        }

//        ipinBarUserDao.save(ipinBarUser);

    }

    private void onActivationCodeMessageReceive(String activationCode, Integer id, Long chatId) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id.longValue());
        InlineKeyboardMarkup markup = inlineView.getRegisterView(States.PERSONALPRODUCTOWNER, States.CITY, States.FINISHED);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.FINISHED);
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.REGISTER_CONFIRM, chatId, markup);
    }

    private void onCityMessageReceive(String city, Integer id, Long chatId) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id.longValue());
        InlineKeyboardMarkup markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.CITY, States.FINISHED);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.ACTIVATION_CODE);
        ipinBarUser.setCity(city);
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.ACTIVATION_CODE, chatId, markup);
    }

    private void onProvinceMessageReceive(String province, Integer id, Long chatId) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id.longValue());
        InlineKeyboardMarkup markup = inlineView.getProvinceView(States.PERSONALPRODUCTOWNER, States.PROVINCE, States.FINISHED);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.CITY);
        ipinBarUser.setCity(province);
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.REGISTER_END, chatId, markup);
    }

    private void onNationalIdentifierMessageReceive(String nationalIdentifer, Integer id, Long chatId) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id.longValue());
        InlineKeyboardMarkup markup = inlineView.getProvinceView(States.PERSONALPRODUCTOWNER, States.NATIONAIDENTIFIER, States.CITY);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.PROVINCE);
        ipinBarUser.setName(nationalIdentifer);
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.PROVINCE, chatId, markup);
    }

    private void onNameFamilyMessageReceive(String nameAndFamily, Integer id, Long chatId) {
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id.longValue());
        InlineKeyboardMarkup markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.NAME, States.NATIONAIDENTIFIER);
        ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.PHONE);
        ipinBarUser.setName(nameAndFamily);
        ipinBarUserDao.save(ipinBarUser);
        replayMessage(ConstantsText.PHONE, chatId, markup);
        changeMainMenu(ConstantsText.PHONE, chatId, KeyboardMenuMaker.contactMenue());

    }

    private void personalProductOwnerCallback(CallbackQuery callbackquery, String state) {
        String replayText = "";
        InlineKeyboardMarkup markup = null;
        long id = callbackquery.getFrom().getId().longValue();
        IpinBarUser ipinBarUser = ipinBarUserDao.findOne(id);
        if (ipinBarUser == null) {
            ipinBarUser = new IpinBarUser(id);
        }
        if (state.equals(States.NAME.name())) {
            replayText = ConstantsText.NAME;
            markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.START, States.PHONE);
            ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.NAME);
        } else if (state.equals(States.PHONE.name())) {
            replayText = ConstantsText.PHONE;
            markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.NAME, States.NATIONAIDENTIFIER);
            ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.PHONE);
            changeMainMenu(replayText, callbackquery.getMessage().getChatId(), KeyboardMenuMaker.contactMenue());
        } else if (state.equals(States.NATIONAIDENTIFIER.name())) {
            replayText = ConstantsText.NATIONALIDENTIFER;
            markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.PHONE, States.PROVINCE);
            ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.NATIONAIDENTIFIER);
        } else if (state.equals(States.PROVINCE.name())) {
            replayText = ConstantsText.PROVINCE;
            markup = inlineView.getProvinceView(States.PERSONALPRODUCTOWNER, States.NATIONAIDENTIFIER, States.CITY);
            ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.PROVINCE);
        } else if (state.startsWith(States.CITY.name())) {
            replayText = ConstantsText.CITY;
            String province[] = state.split(",");
            markup = inlineView.getCityView(States.PERSONALPRODUCTOWNER, States.PROVINCE, States.ACTIVATION_CODE, province[1]);
            ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.CITY);
            ipinBarUser.setProvince(province[1]);
        } else if (state.startsWith(States.ACTIVATION_CODE.name())) {
            replayText = ConstantsText.ACTIVATION_CODE;
            markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.CITY, States.FINISHED);
            ServerResponseCode resp = ipinbarServices.sendActivationCode(ipinBarUser.getPhone(), ipinBarUser.getNationalIdentifier());
            if (resp == ServerResponseCode.DONE) {
                ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.ACTIVATION_CODE);
            }
        } else {
            replayText = ConstantsText.REGISTER_END;
            markup = inlineView.getDirectionView(States.PERSONALPRODUCTOWNER, States.PROVINCE, States.FINISHED);
            ipinBarUser.setCurrentState(States.PERSONALPRODUCTOWNER + ":" + States.FINISHED);
            ipinBarUser.setPersonalOwnerRegister(true);
            ipinBarUser.setCity(state);
        }
        inlineMessageReplay(callbackquery.getMessage().getChatId(), callbackquery.getInlineMessageId(), replayText, callbackquery.getMessage().getMessageId(), markup);
        ipinBarUserDao.save(ipinBarUser);
    }


    private void onLegalDriverClick(Message message) {
    }


    private void onPersonalDriverClic(Message message) {
    }


    private void onProductIndicateClick(Message message) {

    }

    private void onPersonalProductOwnerClick(Message message) {
        UserStateMap.getInstance().replace(message.getFrom().getId(), States.REGISTER);
        replayMessage(ConstantsText.REGISTER_START_TXT, message.getChatId(), inlineView.getStartView(States.PERSONALPRODUCTOWNER, States.NAME));

    }


    private boolean isValidPhoneNumber(String text) {
        if (text.length() == 11 && text.startsWith("09")) {
            return true;
        } else
            return false;
    }


    @Override
    public String getBotUsername() {
        return BotConfig.COMMANDS_USER;
    }

    @Override
    public String getBotToken() {
        return BotConfig.COMMANDS_TOKEN;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    private void replayMessage(String msg, Long chatId, InlineKeyboardMarkup inlineKeyboardMarkup) {
        deleteBotLastMessage(chatId);
        SendMessage message = new SendMessage();
        message.enableMarkdown(true);
        message.setChatId(chatId);
        message.enableMarkdown(true);
        if (inlineKeyboardMarkup != null) {
            message.setReplyMarkup(inlineKeyboardMarkup);
        }

        message.setText(msg);
        try {
            Message replayMsg = sendMessage(message);
            messageId = replayMsg.getMessageId();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void changeMainMenu(String msg, Long chatId, ReplyKeyboardMarkup keyboardMarkup) {
        deleteBotLastMessage(chatId);
        SendMessage message = new SendMessage();
        message.enableMarkdown(true);
        message.setChatId(chatId);
        message.enableMarkdown(true);
        message.setReplyMarkup(keyboardMarkup);
        message.setText(msg);
        try {
            Message replayMsg = sendMessage(message);
            messageId = replayMsg.getMessageId();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void deleteBotLastMessage(Long chatId) {
        if (messageId != null) {
            DeleteMessage dm = new DeleteMessage(chatId, messageId);
            try {
                deleteMessage(dm);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    private void inlineMessageReplay(Long chatId, String inlineMessageId, String msg, Integer messageId, InlineKeyboardMarkup markup) {
        EditMessageText editMarkup = new EditMessageText();
        editMarkup.setChatId(chatId);
        editMarkup.setInlineMessageId(inlineMessageId);
        editMarkup.setText(msg);
        editMarkup.enableMarkdown(true);
        editMarkup.setMessageId(messageId);
        editMarkup.setReplyMarkup(markup);
        try {
            editMessageText(editMarkup);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}