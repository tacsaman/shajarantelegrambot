package net.shajaran.bot.updateshandlers;

import net.shajaran.bot.api.City;
import net.shajaran.bot.api.Province;
import net.shajaran.bot.services.IpinbarServices;
import net.shajaran.bot.structure.States;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.AccessType;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saman on 12/27/2017.
 */
@Component
public class InlineKeyboardMenuMaker {
    Logger logger = Logger.getLogger(InlineKeyboardMenuMaker.class);
    @Autowired
    private IpinbarServices ipinbarServices = null;

    public InlineKeyboardMarkup getStartView(States headState, States start) {
        String startData = headState.name() + ":" + start.name();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline2 = new ArrayList<>();
        rowInline2.add(new InlineKeyboardButton().setText(ConstantsText.REGISTER).setCallbackData(startData));
        rowsInline.add(rowInline2);
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    public InlineKeyboardMarkup getDirectionView(States headState, States back, States next) {
        String backData = headState.name() + ":" + back.name();
        String nextData = headState.name() + ":" + next.name();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline2 = new ArrayList<>();
        rowInline2.add(new InlineKeyboardButton().setText(ConstantsText.BACK).setCallbackData(backData));
        rowInline2.add(new InlineKeyboardButton().setText(ConstantsText.MAINMENUE).setCallbackData(States.MAINMENUE.name()));
//        rowInline2.add(new InlineKeyboardButton().setText(ConstantsText.NEXT).setCallbackData(nextData));
        rowsInline.add(rowInline2);
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    public InlineKeyboardMarkup getProvinceView(States headState, States back, States next) {
        String backData = headState.name() + ":" + back.name();
        String nextData = headState.name() + ":" + next.name();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        Province provinces[] = ipinbarServices.getProvinces();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInlineend1 = null;
        String s = States.PERSONALPRODUCTOWNER.name() + ":" + States.CITY.name() + ",";
        try {
            for (int i = 0; i < provinces.length; i++) {
                rowInlineend1 = new ArrayList<>();
                rowInlineend1.add(new InlineKeyboardButton().setText(provinces[i].getTitle()).setCallbackData(s + provinces[i].getId()));
                rowInlineend1.add(new InlineKeyboardButton().setText(provinces[++i].getTitle()).setCallbackData(s + provinces[i].getId()));
                rowInlineend1.add(new InlineKeyboardButton().setText(provinces[++i].getTitle()).setCallbackData(s + provinces[i].getId()));
                rowInlineend1.add(new InlineKeyboardButton().setText(provinces[++i].getTitle()).setCallbackData(s + provinces[i].getId()));
                rowsInline.add(rowInlineend1);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            rowsInline.add(rowInlineend1);
            logger.error(e);
        }
        rowInlineend1 = new ArrayList<>();
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.BACK).setCallbackData(backData));
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.NEXT).setCallbackData(nextData));
        rowsInline.add(rowInlineend1);

        markupInline.setKeyboard(rowsInline);
        return markupInline;

    }

    public InlineKeyboardMarkup getRegisterView(States headState, States back, States next){
        String backData = headState.name() + ":" + back.name();
        String nextData = headState.name() + ":" + next.name();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInlineend1 = null;

        rowInlineend1 = new ArrayList<>();
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.BACK).setCallbackData(backData));
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.MAINMENUE).setCallbackData(States.MAINMENUE.name()));
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.REGISTER).setCallbackData(nextData));
        rowsInline.add(rowInlineend1);

        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    public InlineKeyboardMarkup getCityView(States headState, States back, States next,String provinceId) {
        String backData = headState.name() + ":" + back.name();
        String nextData = headState.name() + ":" + next.name();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        City citis[] = ipinbarServices.getCitis(provinceId);

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInlineend1 = null;
        String head = States.PERSONALPRODUCTOWNER.name() + ":" + States.ACTIVATION_CODE.name() + ",";
        try {
            for (int i = 0; i < citis.length; i++) {
                rowInlineend1 = new ArrayList<>();
                rowInlineend1.add(new InlineKeyboardButton().setText(citis[i].getTitle()).setCallbackData(head + citis[i].getId()));
                rowInlineend1.add(new InlineKeyboardButton().setText(citis[++i].getTitle()).setCallbackData(head + citis[i].getId()));
                rowInlineend1.add(new InlineKeyboardButton().setText(citis[++i].getTitle()).setCallbackData(head + citis[i].getId()));
                rowInlineend1.add(new InlineKeyboardButton().setText(citis[++i].getTitle()).setCallbackData(head + citis[i].getId()));
                rowsInline.add(rowInlineend1);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            rowsInline.add(rowInlineend1);
            logger.error(e);
        }
        rowInlineend1 = new ArrayList<>();
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.BACK).setCallbackData(backData));
        rowInlineend1.add(new InlineKeyboardButton().setText(ConstantsText.NEXT).setCallbackData(nextData));
        rowsInline.add(rowInlineend1);

        markupInline.setKeyboard(rowsInline);
        return markupInline;

    }

}
