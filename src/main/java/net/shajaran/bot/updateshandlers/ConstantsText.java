package net.shajaran.bot.updateshandlers;

/**
 * Created by saman on 12/16/2017.
 */
public class ConstantsText {

    // Main menu
    public static final String LEGALPRODUCTOWNER = "صاحب بار حقوقی";
    public static final String PERSONALPRODUCTOWNER = "صاحب بار حقیقی";
    public static final String LEGALDRIVER = "راننده حقوقی";
    public static final String PERSONALDRIVER = "راننده حقیقی";
    public static final String APPDOWNLOAD = "دانلود برنامه موبایل";
    public static final String PRODUCTINDICATE = "اعلام بار";
    public static final String FINDPRODUCT = "جستوجوی بار";

    //Wellcom text
    public static final String WELLCOMEMSG = "به شبکه جامع رانندگان (شجران) خوش آمیدید." + "\n" + "من ربات آی پین بار هستم.";
    public static final String WELLCOMEMSG2 = "اگر صاحب بار هستید و دنبال راننده هستید ...";

    public static final String BACK = "برگشت";
    public static final String NEXT = "بعدی";
    public static final String REGISTER="ثبت";
    public static final String MAINMENUE = "منوی اصلی";
    public static final String MAINMENU_SELECT= "گزینه مورد نظر را از منوی اصلی انتخاب کنید";
    public static final String NAME = "نام و نام خانوادگی را وارد کنید.";
    public static final String PHONE = "لطفا شماره تلفن خود را وارد و یا از منوی زیر انتخاب کنید.";
    public static final String CURRENT_PHONE = "تلفن جاری";
    public static final String NATIONALIDENTIFER = "لطفا شماره ملی خود را وارد کنید.";
    public static final String PROVINCE = "استان محل سکونت را انتخاب کنید.";
    public static final String CITY = " شهرستان مورد نظر را انتخاب کنید ";
    public static final String REGISTER_START = "شروع ثبت نام";
    public static final String REGISTER_START_TXT = "برای شروع ثبت نام بر روی دکمه شروع کلیک کنید.";
    public static final String ACTIVATION_CODE="کد فعال سازی ارسال شده را وارد کنید.";
    public static final String REGISTER_END = "ثبت نام با موفقیت انجام شد";
    public static final String REGISTER_CONFIRM = "برای تایید ثبت نام لطفا دکمه ثبت را انتخاب کنید.";

    //Province sate
    public static final String TEHRAN = "تهران";
    public static final String KURDISTAN = "کردستان";
    public static final String ZANJAN = "رنجان";
    public static final String AZARBAYJANSHARGHI = "آذربایجان شرقی";
    public static final String AZARBAYJANGHARBI = "آذربایجان غربی";
    public static final String KERMANSHAH = "کرمانشاه";
    public static final String ILAM = "ایلام";
    public static final String HORMOZAGAN = "هرمزگان";
    public static final String KHUZESTAN = "خوزستان";
    public static final String GHOM = "قم";
    public static final String GHAZVIN = "قزوین";
    public static final String GILAN = "گیلان";
    public static final String LORESTAN = "لرستان";
    public static final String MARKAZI = "مرکزی";
    public static final String SISTANVABALOCHESTAN = "سیستان و بلوچستان";
    public static final String HAMEDAN = "همدان";
    public static final String ARDABIL = "اردبیل";
    public static final String ALBORZ = "البرز";
    public static final String KOHKILOIE = "کهکبلویه و بویر احمد";
    public static final String ESFAHAN = "اصفهان";
    public static final String FARS = "فارس";
    public static final String SEMNAN = "سمنان";
    public static final String YAZD = "یزد";
    public static final String BOSHEHR = "بوشهر";
    public static final String KHURASANSHOMALI = "خراسان شمالی";
    public static final String KURASANRAZAVI = "خراسان رضوی";
    public static final String MAZANARAN = "مازندران";
}
