package net.shajaran.bot.updateshandlers;

import net.shajaran.bot.structure.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saman on 12/9/2017.
 */
@Component
public class KeyboardMenuMaker implements ApplicationContextAware {

    private ApplicationContext context = null;
    IpinBarUserDao ipinBarUserDao;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    public static ReplyKeyboardMarkup mainMenu() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);


        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(ConstantsText.LEGALPRODUCTOWNER);
        keyboardFirstRow.add(ConstantsText.PERSONALPRODUCTOWNER);
        KeyboardRow keyBoardSecondRow = new KeyboardRow();
        keyBoardSecondRow.add(ConstantsText.LEGALDRIVER);
        keyBoardSecondRow.add(ConstantsText.PERSONALDRIVER);
        KeyboardRow keyBoardThirdRow = new KeyboardRow();
        keyBoardThirdRow.add(ConstantsText.PRODUCTINDICATE);
        keyBoardThirdRow.add(ConstantsText.FINDPRODUCT);
        KeyboardRow keyBoardForthRow = new KeyboardRow();
        keyBoardForthRow.add(ConstantsText.APPDOWNLOAD);
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyBoardSecondRow);
        keyboard.add(keyBoardThirdRow);
        keyboard.add(keyBoardForthRow);
        replyKeyboardMarkup.setKeyboard(keyboard);

        return replyKeyboardMarkup;
    }


    public static ReplyKeyboardMarkup contactMenue() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardButton contact = new KeyboardButton();
        contact.setText(ConstantsText.CURRENT_PHONE);
        contact.setRequestContact(true);
        keyboardFirstRow.add(contact);

        keyboard.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboard);

        return replyKeyboardMarkup;
    }


}
