package net.shajaran.bot.structure;

import java.util.HashMap;

/**
 * Created by saman on 12/12/2017.
 */
public class IpinBarUsersMap extends HashMap<Integer,IpinBarUser> {
    public static IpinBarUsersMap usersMap = new IpinBarUsersMap();

    private IpinBarUsersMap()
    {}

    public static IpinBarUsersMap getInstance()
    {
        return usersMap;
    }
}
