package net.shajaran.bot.structure;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by saman on 12/12/2017.
 */
@Entity
@Table(name = "ipinUsers")
public class IpinBarUser {
    @Id
    long id;

    String name;
    String phone;
    String province;
    String city;
    String nationalIdentifier;
    String currentState;
    boolean ownerRegister = false;
    boolean driverRegister = false;
    boolean downloadApp = false;

    public IpinBarUser() {
    }

    public IpinBarUser(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public boolean isOwnerRegister() {
        return ownerRegister;
    }

    public void setPersonalOwnerRegister(boolean ownerRegister) {
        this.ownerRegister = ownerRegister;
    }

    public boolean isDriverRegister() {
        return driverRegister;
    }

    public void setDriverRegister(boolean driverRegister) {
        this.driverRegister = driverRegister;
    }

    public boolean isDownloadApp() {
        return downloadApp;
    }

    public void setDownloadApp(boolean downloadApp) {
        this.downloadApp = downloadApp;
    }

    public String getNationalIdentifier() {
        return nationalIdentifier;
    }

    public void setNationalIdentifier(String nationalIdentifier) {
        this.nationalIdentifier = nationalIdentifier;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "[ name: " + this.getName() + ", phone: " + this.getPhone() + " ]";
    }
}
