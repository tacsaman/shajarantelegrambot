package net.shajaran.bot.structure;

/**
 * Created by saman on 12/27/2017.
 */
public enum  ServerResponseCode {
    DONE("با موفقیت انجام شد"), ERROR("عملیات با خطا مواجه شده است");
    private final String response;

    ServerResponseCode(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return response;
    }
}
