package net.shajaran.bot.structure;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IpinBarUserDao extends CrudRepository<IpinBarUser,Long> {
}
