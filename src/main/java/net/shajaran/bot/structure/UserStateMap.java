package net.shajaran.bot.structure;

import java.util.HashMap;

/**
 * Created by saman on 12/12/2017.
 */
public class UserStateMap extends HashMap<Integer, States> {
    public static UserStateMap map= new UserStateMap();

    private UserStateMap() {
    }

    public static HashMap<Integer, States> getInstance() {
      return map;
    }
}
