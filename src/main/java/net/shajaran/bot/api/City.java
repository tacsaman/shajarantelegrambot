package net.shajaran.bot.api;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;

/**
 * Created by sina on 2/8/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class City {

    GoogleMap googleMap = new GoogleMap();
    @Id
    private String id;
    private String title;
    private String googleLongNameEN;
    private String googleShortNameEN;
    private String googleLongNameFA;
    private String googleShortNameFA;
    private Province province;


    public City() {
    }

    public City(String id) {
        this.id = id;
    }
    //    public Point getPoint() {
//        return point;
//    }
//
//    public void setPoint(Point point) {
//        this.point = point;
//    }

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    public void setGoogleMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public String getGoogleLongNameEN() {
        return googleLongNameEN;
    }

    public void setGoogleLongNameEN(String googleLongNameEN) {
        this.googleLongNameEN = googleLongNameEN;
    }

    public String getGoogleShortNameEN() {
        return googleShortNameEN;
    }

    public void setGoogleShortNameEN(String googleShortNameEN) {
        this.googleShortNameEN = googleShortNameEN;
    }

    public String getGoogleLongNameFA() {
        return googleLongNameFA;
    }

    public void setGoogleLongNameFA(String googleLongNameFA) {
        this.googleLongNameFA = googleLongNameFA;
    }

    public String getGoogleShortNameFA() {
        return googleShortNameFA;
    }

    public void setGoogleShortNameFA(String googleShortNameFA) {
        this.googleShortNameFA = googleShortNameFA;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonBackReference
    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }
}
