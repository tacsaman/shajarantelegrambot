/*
package net.shajaran.bot.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.sayar.vehicleresource.enums.CeoStatus;
import ir.sayar.vehicleresource.enums.Gender;
import ir.sayar.vehicleresource.enums.UserRank;
import ir.sayar.vehicleresource.enums.UserStatus;
import ir.sayar.vehicleresource.model.Image.Image;
import ir.sayar.vehicleresource.model.educationInformation.Education;
import ir.sayar.vehicleresource.model.personInformation.address.Address;
import ir.sayar.vehicleresource.security.model.UserAccount;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


*/
/**
 * Created by sina on 2/8/17.
 *//*

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person implements Serializable {
    @Id
    private String id;
    private String bcno;
    private Account account;
    private UserAccount userAccount;
    private String companyId;
    private Double credit;
    private Double gift;
    private String provinceAdminId;
    private UserRank userRank;
    private String confirmationDate;
    private Boolean confirmationPersonInformation; //  تایید اطلاعات فردی
    private Boolean confirmationPersonAddress; //  تایید اطلاعات ادرس
    private Boolean confirmationPersonAccount; //  تایید اطلاعات بانکی
    private Boolean confirmationPersonlicenceCodeDriver; //  تایید اطلاعات گواهینامه
    private String confirmThisFormId; // ای دی فردی که فرم را ذخیره تایید کرده
    private List<DefaultAddress> defaultAddressList = new ArrayList<>();
    private List<DefaultAddress> defaultAddressDestinationList = new ArrayList<>();
    private UserStatus userStatus;
    private UserStatus userStatusBefore;
    private String userActivationToken;
    private Boolean legal; // پیش فرض حقیقی اگر حقوقی بود true می شود
    private Boolean createWeb;
    private Boolean amountFor10Trip;
    private Boolean amountFor25Trip;
    private Boolean amountFor50Trip;
    private Integer discountTripCount;
//    @Indexed
    private String code;
    @Indexed
    private String presentedByCode;
    private CeoStatus ceoStatus = CeoStatus.NULL;

    private String firstNameFa;
    private String firstNameEn;
    private String lastNameFa;
    private String lastNameEn;
    private String fatherNameFa;
    private String fatherNameEn;
    private String identityId;
    private Gender gender;
    private Image photo;
    private String birthDate;
    private String passPortNo;
    @DBRef
    private List<Education> educationList;
    private Contact contact;
    private Address address;
    private String email;

    private String phone;
    private String phone2;
    @Indexed
    private Boolean active;
    private String fcmToken;
    private Image nationalCard;
    private Image nationalCardBack;

    public Person() {
        active = false;
        fcmToken = null;
        credit = 10000.0;
        gift = 10000.0;
        confirmationPersonInformation = false;
        confirmationPersonAddress = false;
        confirmationPersonAccount = false;
        confirmationPersonlicenceCodeDriver = false;
        legal = false;
        amountFor10Trip = false;
        amountFor25Trip = false;
        amountFor50Trip = false;
        discountTripCount = 0;
        userStatus = UserStatus.REGISTERED;
    }

    public Person(String id) {
        this.id = id;
    }

    public CeoStatus getCeoStatus() {
        return ceoStatus;
    }

    public void setCeoStatus(CeoStatus ceoStatus) {
        this.ceoStatus = ceoStatus;
    }

    public Boolean getAmountFor10Trip() {
        return amountFor10Trip;
    }

    public void setAmountFor10Trip(Boolean amountFor10Trip) {
        this.amountFor10Trip = amountFor10Trip;
    }

    public Boolean getAmountFor25Trip() {
        return amountFor25Trip;
    }

    public void setAmountFor25Trip(Boolean amountFor25Trip) {
        this.amountFor25Trip = amountFor25Trip;
    }

    public Boolean getAmountFor50Trip() {
        return amountFor50Trip;
    }

    public void setAmountFor50Trip(Boolean amountFor50Trip) {
        this.amountFor50Trip = amountFor50Trip;
    }

    public Integer getDiscountTripCount() {
        return discountTripCount;
    }

    public void setDiscountTripCount(Integer discountTripCount) {
        this.discountTripCount = discountTripCount;
    }

    public Double getGift() {
        return gift;
    }

    public void setGift(Double gift) {
        this.gift = gift;
    }


    public String getPresentedByCode() {
        return presentedByCode;
    }

    public void setPresentedByCode(String presentedByCode) {
        this.presentedByCode = presentedByCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getCreateWeb() {
        return createWeb;
    }

    public void setCreateWeb(Boolean createWeb) {
        this.createWeb = createWeb;
    }

    public Boolean getLegal() {
        return legal;
    }

    public void setLegal(Boolean legal) {
        this.legal = legal;
    }

    public String getUserActivationToken() {
        return userActivationToken;
    }

    public void setUserActivationToken(String userActivationToken) {
        this.userActivationToken = userActivationToken;
    }

    public UserStatus getUserStatusBefore() {
        return userStatusBefore;
    }

    public void setUserStatusBefore(UserStatus userStatusBefore) {
        this.userStatusBefore = userStatusBefore;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public List<DefaultAddress> getDefaultAddressDestinationList() {
        return defaultAddressDestinationList;
    }

    public void setDefaultAddressDestinationList(List<DefaultAddress> defaultAddressDestinationList) {
        this.defaultAddressDestinationList = defaultAddressDestinationList;
    }

    public List<DefaultAddress> getDefaultAddressList() {
        return defaultAddressList;
    }

    public void setDefaultAddressList(List<DefaultAddress> defaultAddressList) {
        this.defaultAddressList = defaultAddressList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstNameFa() {
        return firstNameFa;
    }

    public void setFirstNameFa(String firstNameFa) {
        this.firstNameFa = firstNameFa;
    }

    public String getFirstNameEn() {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn) {
        this.firstNameEn = firstNameEn;
    }

    public String getLastNameFa() {
        return lastNameFa;
    }

    public void setLastNameFa(String lastNameFa) {
        this.lastNameFa = lastNameFa;
    }

    public String getLastNameEn() {
        return lastNameEn;
    }

    public void setLastNameEn(String lastNameEn) {
        this.lastNameEn = lastNameEn;
    }

    public String getFatherNameFa() {
        return fatherNameFa;
    }

    public void setFatherNameFa(String fatherNameFa) {
        this.fatherNameFa = fatherNameFa;
    }

    public String getFatherNameEn() {
        return fatherNameEn;
    }

    public void setFatherNameEn(String fatherNameEn) {
        this.fatherNameEn = fatherNameEn;
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public String getBcno() {
        return bcno;
    }

    public void setBcno(String bcno) {
        this.bcno = bcno;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPassPortNo() {
        return passPortNo;
    }

    public void setPassPortNo(String passPortNo) {
        this.passPortNo = passPortNo;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public String getProvinceAdminId() {
        return provinceAdminId;
    }

    public void setProvinceAdminId(String provinceAdminId) {
        this.provinceAdminId = provinceAdminId;
    }

    public UserRank getUserRank() {
        return userRank;
    }

    public void setUserRank(UserRank userRank) {
        this.userRank = userRank;
    }

    public Boolean getConfirmationPersonInformation() {
        return confirmationPersonInformation;
    }

    public void setConfirmationPersonInformation(Boolean confirmationPersonInformation) {
        this.confirmationPersonInformation = confirmationPersonInformation;
    }

    public Boolean getConfirmationPersonAddress() {
        return confirmationPersonAddress;
    }

    public void setConfirmationPersonAddress(Boolean confirmationPersonAddress) {
        this.confirmationPersonAddress = confirmationPersonAddress;
    }

    public Boolean getConfirmationPersonAccount() {
        return confirmationPersonAccount;
    }

    public void setConfirmationPersonAccount(Boolean confirmationPersonAccount) {
        this.confirmationPersonAccount = confirmationPersonAccount;
    }

    public Boolean getConfirmationPersonlicenceCodeDriver() {
        return confirmationPersonlicenceCodeDriver;
    }

    public void setConfirmationPersonlicenceCodeDriver(Boolean confirmationPersonlicenceCodeDriver) {
        this.confirmationPersonlicenceCodeDriver = confirmationPersonlicenceCodeDriver;
    }

    public String getConfirmThisFormId() {
        return confirmThisFormId;
    }

    public void setConfirmThisFormId(String confirmThisFormId) {
        this.confirmThisFormId = confirmThisFormId;
    }

    public Image getNationalCard() {
        return nationalCard;
    }

    public void setNationalCard(Image nationalCard) {
        this.nationalCard = nationalCard;
    }

    public Image getNationalCardBack() {
        return nationalCardBack;
    }

    public void setNationalCardBack(Image nationalCardBack) {
        this.nationalCardBack = nationalCardBack;
    }
}
*/
