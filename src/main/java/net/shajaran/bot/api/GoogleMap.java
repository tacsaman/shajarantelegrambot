package net.shajaran.bot.api;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by sina on 2/8/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GoogleMap {
    private Double latitude;
    private Double longitude;

    public GoogleMap() {
    }

    public GoogleMap(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
