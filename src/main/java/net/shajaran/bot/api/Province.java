package net.shajaran.bot.api;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sina on 2/8/17.
 */
public class Province {
    private String id;
    private String title;
    private String googleLongNameEN;
    private String googleLongNameFA;
    private String googleShortNameEN;
    private String googleShortNameFA;
    private GoogleMap googleMap = new GoogleMap();
    private String code;//we determine code for each province
    private List<City> cityList = new ArrayList<>();

    public Province() {
    }


    public Province(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    public void setGoogleMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    public String getGoogleLongNameEN() {
        return googleLongNameEN;
    }

    public void setGoogleLongNameEN(String googleLongNameEN) {
        this.googleLongNameEN = googleLongNameEN;
    }

    public String getGoogleShortNameEN() {
        return googleShortNameEN;
    }

    public void setGoogleShortNameEN(String googleShortNameEN) {
        this.googleShortNameEN = googleShortNameEN;
    }

    public String getGoogleLongNameFA() {
        return googleLongNameFA;
    }

    public void setGoogleLongNameFA(String googleLongNameFA) {
        this.googleLongNameFA = googleLongNameFA;
    }

    public String getGoogleShortNameFA() {
        return googleShortNameFA;
    }

    public void setGoogleShortNameFA(String googleShortNameFA) {
        this.googleShortNameFA = googleShortNameFA;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonManagedReference
    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }
}
