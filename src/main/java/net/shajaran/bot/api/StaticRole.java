package net.shajaran.bot.api;

/**
 * Created by yaqub on 6/15/17.
 */
public enum StaticRole {
    ADMIN,
    PROVINCE_ADMIN,

    DRIVER_BRONZE,
    DRIVER_SILVER,
//    DRIVER_GOLD,

    COMPANY_BRONZE,
    COMPANY_SILVER,
//    COMPANY_GOLD,


    GOODS_OWNER_BRONZE,
    GOODS_OWNER_SILVER,
//    GOODS_OWNER_GOLD,

    CUSTOMER,

    MARKETER_BRONZE,
    MARKETER_SILVER
}
