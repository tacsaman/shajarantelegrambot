package net.shajaran.bot.services;

import com.google.gson.Gson;
import net.shajaran.bot.api.City;
import net.shajaran.bot.api.Province;
import net.shajaran.bot.structure.ServerResponseCode;
import net.shajaran.bot.structure.States;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saman on 12/18/2017.
 */
@Service
public class IpinbarServices {

    @Autowired
    Environment environment;

    Logger logger = Logger.getLogger(IpinbarServices.class);
    public   String baseUrl;
    private Province[] provinces;
    private City[] cities;

    @PostConstruct
    private void init(){
        baseUrl= environment.getProperty("server.base.url");
        provinces = getProvince();
    }


    public Province[] getProvinces() {
        return provinces;
    }

    public Province[] getProvince() {
        try {
            String path = baseUrl + environment.getProperty("server.province.url");
            URL url = new URL(path);
            JSONObject object = new JSONObject(getJsonString(url));
            Gson gson = new Gson();
            provinces = gson.fromJson(object.get("data").toString(), Province[].class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return provinces;
    }

    public City[] getCitis(String provinceId) {
        try {
            String path = baseUrl + environment.getProperty("server.city.url") + provinceId + "/idTitle";
            URL url = new URL(path);
            JSONObject object = new JSONObject(getJsonString(url));
            Gson gson = new Gson();
            cities = gson.fromJson(object.get("data").toString(), City[].class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cities;
    }

    public ServerResponseCode sendActivationCode(String phoneNumber, String nationalId)
    {
        Object response="";
        try {
            String path = baseUrl + environment.getProperty("server.sendActivationCode.url") + "/"+phoneNumber + "/"+phoneNumber + "/"+ nationalId;
            URL url = new URL(path);
            JSONObject object = new JSONObject(getJsonString(url));
            Gson gson = new Gson();
             response = gson.fromJson(object.get("data").toString(), Object.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return responseMaper(response);
    }

    private ServerResponseCode responseMaper(Object response) {
        logger.info("ActivationCode Response:" + response);
        if (response.equals("DONE"))
        {
            return ServerResponseCode.DONE;
        }
        return ServerResponseCode.ERROR;
    }

    public City getCityById(String provinceId,String cityId)
    {
        City[] cities = getCitis(provinceId);
        for(City city:cities)
        {
            if (city.getId().equals(city))
                return city;
        }
        return null;
    }


    private String getJsonString(URL url) {

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");
            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            logger.info("\nSending 'GET' request to URL : " + url);
            logger.info("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            return response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args) throws IOException {
        IpinbarServices ipinbarServices = new IpinbarServices();
        ipinbarServices.sendActivationCode("09120172048","3721238494");


    }
}
