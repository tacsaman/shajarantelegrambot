package net.shajaran.bot.commands;

import net.shajaran.bot.updateshandlers.KeyboardMenuMaker;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.groupadministration.GetChatMember;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 * This command simply replies with a hello to the users command and
 * sends them the 'kind' words back, which they send via command parameters
 *
 * @author Timo Schulz (Mit0x2)
 */
@Component
public class MemberCommand extends BotCommand implements ApplicationContextAware {

    private static final String LOGTAG = "HELLOCOMMAND";
    private ApplicationContext context;

    public MemberCommand() {
        super("member", "عضویت در کانال ای پین بار");
    }


    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {

        KeyboardMenuMaker mainMenuHandler = context.getBean(KeyboardMenuMaker.class);
        String userName = chat.getUserName();
        if (userName == null || userName.isEmpty()) {
            userName = user.getFirstName();
        }

        GetChatMember chatMember = new GetChatMember();
        chatMember.setChatId("@ipinbar");
        chatMember.setUserId(user.getId());

       /* ChatMember t = null;
        try {
            t = absSender.getChatMember(chatMember);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }*/
        StringBuilder messageTextBuilder;

//            absSender.sendMessage(mainMenuHandler.makeMainMenuKeyboard(chat.getId()));

        /*if (!t.getStatus().equalsIgnoreCase("member") && !t.getStatus().equalsIgnoreCase("administrator"))
        {
            messageTextBuilder = new StringBuilder("عزیز").append(userName);

                messageTextBuilder.append("\n");
                messageTextBuilder.append("لطفا برای استفاده از شبکه جامع رانندگان ابتدا در کانال  ").append(" @ipinbar ").append("عضو شوید.");
                messageTextBuilder.append(String.join(" ", arguments));
        }else {
            messageTextBuilder = new StringBuilder(DatabaseManager.getInstance().insertIpinUser());
        }
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setText(messageTextBuilder.toString());
        try {
            absSender.sendMessage(answer);
        } catch (TelegramApiException e) {
            BotLogger.error(LOGTAG, e);
        }*/
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}