package net.shajaran.bot.commands;

import net.shajaran.bot.structure.Emoji;
import net.shajaran.bot.structure.IpinBarUsersMap;
import net.shajaran.bot.structure.States;
import net.shajaran.bot.structure.UserStateMap;
import net.shajaran.bot.updateshandlers.ConstantsText;
import net.shajaran.bot.updateshandlers.KeyboardMenuMaker;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

/**
 * This commands starts the conversation with the bot
 *
 * @author Timo Schulz (Mit0x2)
 */
public class StartCommand extends BotCommand {
 Logger logger = Logger.getLogger(StartCommand.class);

    public StartCommand() {
        super("start", "With this command you can start the Bot");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        logger.info("IpinMap users: "+ IpinBarUsersMap.getInstance().size());
        UserStateMap.getInstance().put(user.getId(), States.START);
        StringBuilder messageBuilder = new StringBuilder();
        String userName = user.getFirstName();
        messageBuilder.append("سلام ").append(userName).append("\n");
        messageBuilder.append(ConstantsText.WELLCOMEMSG).append("\n");
//        messageBuilder.append("برای شروع بر روی ").append(" /shajaran ").append("کلیک کنید.");
        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setText(messageBuilder.toString());

        try {
            absSender.sendMessage(answer);
            absSender.sendMessage(sendStartMessage(chat.getId(),answer.getReplyToMessageId(), KeyboardMenuMaker.mainMenu(),""));
        } catch (TelegramApiException e) {
            logger.error(e);
        }
    }

    private static SendMessage sendStartMessage(Long chatId, Integer messageId, ReplyKeyboardMarkup replyKeyboardMarkup, String language) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setReplyToMessageId(messageId);
        if (replyKeyboardMarkup != null) {
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
        }
        sendMessage.setText(ConstantsText.WELLCOMEMSG2);
        return sendMessage;
    }


}